var jq=jQuery.noConflict();
    jq(document).ready(function(){
        jq("a#js-toggle-menu").on('click touchstart', function(e){
            jq("div#js-menus").addClass('open');
            jq("div#js-shade").addClass('open');
            e.preventDefault();
        });

        jq("div#js-shade").on('touchstart click', function(e){
            jq("div#js-menus").removeClass('open');
            jq("div#js-shade").removeClass('open');
            e.preventDefault();
        });

        jq('div#js-cycle-slideshow').cycle({
            slides: ">div.cycle__item",
            next: "a.cycle__next",
            prev: "a.cycle__prev",
            pager: "div.cycle__pager",
            fx: 'scrollHorz'
        }).cycle('pause');
    });