<?php
include 'shortcodes.php';
include 'options_framework.php';
/**
 * Twenty Thirteen functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see https://codex.wordpress.org/Theme_Development
 * and https://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link https://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
/*
 * Set up the content width value based on the theme's design.
 *
 * @see twentythirteen_content_width() for template-specific adjustments.
 */
if (!isset($content_width))
  $content_width = 604;
/**
 * Add support for a custom header image.
 */
require get_template_directory() . '/inc/custom-header.php';
/**
 * Twenty Thirteen only works in WordPress 3.6 or later.
 */
if (version_compare($GLOBALS['wp_version'], '3.6-alpha', '<'))
  require get_template_directory() . '/inc/back-compat.php';
/**
 * Twenty Thirteen setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Thirteen supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_setup() {
  /*
   * Makes Twenty Thirteen available for translation.
   *
   * Translations can be added to the /languages/ directory.
   * If you're building a theme based on Twenty Thirteen, use a find and
   * replace to change 'twentythirteen' to the name of your theme in all
   * template files.
   */
  load_theme_textdomain('twentythirteen', get_template_directory() . '/languages');

  /*
   * This theme styles the visual editor to resemble the theme style,
   * specifically font, colors, icons, and column width.
   */
  add_editor_style(array('css/editor-style.css', 'genericons/genericons.css', twentythirteen_fonts_url()));

  // Adds RSS feed links to <head> for posts and comments.
  add_theme_support('automatic-feed-links');

  /*
   * Switches default core markup for search form, comment form,
   * and comments to output valid HTML5.
   */
  add_theme_support('html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
  ));

  /*
   * This theme supports all available post formats by default.
   * See https://codex.wordpress.org/Post_Formats
   */
  add_theme_support('post-formats', array(
      'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
  ));

  // This theme uses wp_nav_menu() in one location.
  register_nav_menu('primary', __('Navigation Menu', 'twentythirteen'));

  /*
   * This theme uses a custom image size for featured images, displayed on
   * "standard" posts and pages.
   */
  add_theme_support('post-thumbnails');
  set_post_thumbnail_size(604, 270, true);

  // This theme uses its own gallery styles.
  add_filter('use_default_gallery_style', '__return_false');
}
add_action('after_setup_theme', 'twentythirteen_setup');
/**
 * Return the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function twentythirteen_fonts_url() {
  $fonts_url = '';

  /* Translators: If there are characters in your language that are not
   * supported by Source Sans Pro, translate this to 'off'. Do not translate
   * into your own language.
   */
  $source_sans_pro = _x('on', 'Source Sans Pro font: on or off', 'twentythirteen');

  /* Translators: If there are characters in your language that are not
   * supported by Bitter, translate this to 'off'. Do not translate into your
   * own language.
   */
  $bitter = _x('on', 'Bitter font: on or off', 'twentythirteen');

  if ('off' !== $source_sans_pro || 'off' !== $bitter) {
    $font_families = array();

    if ('off' !== $source_sans_pro)
      $font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

    if ('off' !== $bitter)
      $font_families[] = 'Bitter:400,700';

    $query_args = array(
        'family' => urlencode(implode('|', $font_families)),
        'subset' => urlencode('latin,latin-ext'),
    );
    $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
  }

  return $fonts_url;
}
/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_scripts_styles() {
  /*
   * Adds JavaScript to pages with the comment form to support
   * sites with threaded comments (when in use).
   */
  //if (is_singular() && comments_open() && get_option('thread_comments'))
  //  wp_enqueue_script('comment-reply');

  // Adds Masonry to handle vertical alignment of footer widgets.
  //if (is_active_sidebar('sidebar-1'))
  //  wp_enqueue_script('jquery-masonry');

  // Loads JavaScript file with functionality specific to Twenty Thirteen.
  //wp_enqueue_script('twentythirteen-script', get_template_directory_uri() . '/js/functions.js', array('jquery'), '20150330', true);

  // Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
  //wp_enqueue_style('twentythirteen-fonts', twentythirteen_fonts_url(), array(), null);

  // Add Genericons font, used in the main stylesheet.
  //wp_enqueue_style('genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.03');

  // Loads our main stylesheet.
  //wp_enqueue_style('twentythirteen-style', get_stylesheet_uri(), array(), '2013-07-18');
  // Loads the Internet Explorer specific stylesheet.
  //wp_enqueue_style('twentythirteen-ie', get_template_directory_uri() . '/css/ie.css', array('twentythirteen-style'), '2013-07-18');
  //wp_style_add_data('twentythirteen-ie', 'conditional', 'lt IE 9');


  //wp_register_style( 'custom-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '20120208', 'all' );
  wp_register_style('custom-style-font-awesome', get_template_directory_uri() . '/gl-assets/font-awesome-4.5.0/css/font-awesome.min.css', array(), '20120208', 'all');
  wp_enqueue_style('custom-style-font-awesome');
  wp_register_style('custom-style-font-merriweather', 'https://fonts.googleapis.com/css?family=Merriweather:300,300italic', array(), '20120208', 'all');
  wp_enqueue_style('custom-style-font-merriweather');
  // Register the style like this for a theme:
  wp_register_style('custom-style', get_template_directory_uri() . '/gl-assets/css/app.min.css', array(), '20120208', 'all');
  // For either a plugin or a theme, you can then enqueue the style:
  wp_enqueue_style('custom-style');

//	wp_register_style( 'style2', get_template_directory_uri() . '/css/style2.css', array(), '1', 'all' );
//	wp_enqueue_style( 'style2' );
  // Register the style like this for a theme:

  wp_deregister_script('jquery');
  wp_register_script('jquery', get_template_directory_uri() . '/gl-assets/bower_components/jquery/dist/jquery.min.js', '', 'v2.1.4');
  wp_enqueue_script('jquery');

  wp_enqueue_script('gl-app', get_template_directory_uri() . '/gl-assets/js/gl-app.min.js', array('jquery'), time(), true);
}
add_action('wp_enqueue_scripts', 'twentythirteen_scripts_styles');
/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function twentythirteen_wp_title($title, $sep) {
  global $paged, $page;

  if (is_feed())
    return $title;

  // Add the site name.
  $title .= get_bloginfo('name', 'display');

  // Add the site description for the home/front page.
  $site_description = get_bloginfo('description', 'display');
  if ($site_description && ( is_home() || is_front_page() ))
    $title = "$title $sep $site_description";

  // Add a page number if necessary.
  if (( $paged >= 2 || $page >= 2 ) && !is_404())
    $title = "$title $sep " . sprintf(__('Page %s', 'twentythirteen'), max($paged, $page));

  return $title;
}
add_filter('wp_title', 'twentythirteen_wp_title', 10, 2);
/**
 * Register two widget areas.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_widgets_init() {
  register_sidebar(array(
      'name' => __('Main Widget Area', 'twentythirteen'),
      'id' => 'sidebar-1',
      'description' => __('Appears in the footer section of the site.', 'twentythirteen'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
  ));

  register_sidebar(array(
      'name' => __('Secondary Widget Area', 'twentythirteen'),
      'id' => 'sidebar-2',
      'description' => __('Appears on posts and pages in the sidebar.', 'twentythirteen'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
  ));
}
add_action('widgets_init', 'twentythirteen_widgets_init');
if (!function_exists('twentythirteen_paging_nav')) :

  /**
   * Display navigation to next/previous set of posts when applicable.
   *
   * @since Twenty Thirteen 1.0
   */
  function twentythirteen_paging_nav() {
    global $wp_query;

    // Don't print empty markup if there's only one page.
    if ($wp_query->max_num_pages < 2)
      return;
    ?>
    <nav class="navigation paging-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php _e('Posts navigation', 'twentythirteen'); ?></h1>
      <div class="nav-links">

        <?php if (get_next_posts_link()) : ?>
          <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'twentythirteen')); ?></div>
        <?php endif; ?>

        <?php if (get_previous_posts_link()) : ?>
          <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'twentythirteen')); ?></div>
        <?php endif; ?>

      </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
  }
endif;
if (!function_exists('twentythirteen_post_nav')) :

  /**
   * Display navigation to next/previous post when applicable.
   *
   * @since Twenty Thirteen 1.0
   */
  function twentythirteen_post_nav() {
    global $post;

    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post($post->post_parent) : get_adjacent_post(false, '', true);
    $next = get_adjacent_post(false, '', false);

    if (!$next && !$previous)
      return;
    ?>
    <nav class="navigation post-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php _e('Post navigation', 'twentythirteen'); ?></h1>
      <div class="nav-links">

        <?php previous_post_link('%link', _x('<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'twentythirteen')); ?>
        <?php next_post_link('%link', _x('%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen')); ?>

      </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
  }
endif;
if (!function_exists('twentythirteen_entry_meta')) :

  /**
   * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
   *
   * Create your own twentythirteen_entry_meta() to override in a child theme.
   *
   * @since Twenty Thirteen 1.0
   */
  function twentythirteen_entry_meta() {
    if (is_sticky() && is_home() && !is_paged())
      echo '<span class="featured-post">' . esc_html__('Sticky', 'twentythirteen') . '</span>';

    if (!has_post_format('link') && 'post' == get_post_type())
      twentythirteen_entry_date();

    // Translators: used between list items, there is a space after the comma.
    $categories_list = get_the_category_list(__(', ', 'twentythirteen'));
    if ($categories_list) {
      echo '<span class="categories-links">' . $categories_list . '</span>';
    }

    // Translators: used between list items, there is a space after the comma.
    $tag_list = get_the_tag_list('', __(', ', 'twentythirteen'));
    if ($tag_list) {
      echo '<span class="tags-links">' . $tag_list . '</span>';
    }

    // Post author
    if ('post' == get_post_type()) {
      printf('<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'twentythirteen'), get_the_author())), get_the_author()
      );
    }
  }

endif;
if (!function_exists('twentythirteen_entry_date')) :

  /**
   * Print HTML with date information for current post.
   *
   * Create your own twentythirteen_entry_date() to override in a child theme.
   *
   * @since Twenty Thirteen 1.0
   *
   * @param boolean $echo (optional) Whether to echo the date. Default true.
   * @return string The HTML-formatted post date.
   */
  function twentythirteen_entry_date($echo = true) {
    if (has_post_format(array('chat', 'status')))
      $format_prefix = _x('%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen');
    else
      $format_prefix = '%2$s';

    $date = sprintf('<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>', esc_url(get_permalink()), esc_attr(sprintf(__('Permalink to %s', 'twentythirteen'), the_title_attribute('echo=0'))), esc_attr(get_the_date('c')), esc_html(sprintf($format_prefix, get_post_format_string(get_post_format()), get_the_date()))
    );

    if ($echo)
      echo $date;

    return $date;
  }
endif;
if (!function_exists('twentythirteen_the_attached_image')) :

  /**
   * Print the attached image with a link to the next attached image.
   *
   * @since Twenty Thirteen 1.0
   */
  function twentythirteen_the_attached_image() {
    /**
     * Filter the image attachment size to use.
     *
     * @since Twenty thirteen 1.0
     *
     * @param array $size {
     *     @type int The attachment height in pixels.
     *     @type int The attachment width in pixels.
     * }
     */
    $attachment_size = apply_filters('twentythirteen_attachment_size', array(724, 724));
    $next_attachment_url = wp_get_attachment_url();
    $post = get_post();

    /*
     * Grab the IDs of all the image attachments in a gallery so we can get the URL
     * of the next adjacent image in a gallery, or the first image (if we're
     * looking at the last image in a gallery), or, in a gallery of one, just the
     * link to that image file.
     */
    $attachment_ids = get_posts(array(
        'post_parent' => $post->post_parent,
        'fields' => 'ids',
        'numberposts' => -1,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
    ));

    // If there is more than 1 attachment in a gallery...
    if (count($attachment_ids) > 1) {
      foreach ($attachment_ids as $attachment_id) {
        if ($attachment_id == $post->ID) {
          $next_id = current($attachment_ids);
          break;
        }
      }

      // get the URL of the next image attachment...
      if ($next_id)
        $next_attachment_url = get_attachment_link($next_id);

      // or get the URL of the first image attachment.
      else
        $next_attachment_url = get_attachment_link(reset($attachment_ids));
    }

    printf('<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>', esc_url($next_attachment_url), the_title_attribute(array('echo' => false)), wp_get_attachment_image($post->ID, $attachment_size)
    );
  }
endif;
/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string The Link format URL.
 */
function twentythirteen_get_link_url() {
  $content = get_the_content();
  $has_url = get_url_in_content($content);

  return ( $has_url ) ? $has_url : apply_filters('the_permalink', get_permalink());
}
if (!function_exists('twentythirteen_excerpt_more') && !is_admin()) :

  /**
   * Replaces "[...]" (appended to automatically generated excerpts) with ...
   * and a Continue reading link.
   *
   * @since Twenty Thirteen 1.4
   *
   * @param string $more Default Read More excerpt link.
   * @return string Filtered Read More excerpt link.
   */
  function twentythirteen_excerpt_more($more) {
    $link = sprintf('<a href="%1$s" class="more-link">%2$s</a>', esc_url(get_permalink(get_the_ID())),
            /* translators: %s: Name of current post */ sprintf(__('Continue reading %s <span class="meta-nav">&rarr;</span>', 'twentythirteen'), '<span class="screen-reader-text">' . get_the_title(get_the_ID()) . '</span>')
    );
    return ' &hellip; ' . $link;
  }

  add_filter('excerpt_more', 'twentythirteen_excerpt_more');
endif;
/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentythirteen_body_class($classes) {
  if (!is_multi_author())
    $classes[] = 'single-author';

  if (is_active_sidebar('sidebar-2') && !is_attachment() && !is_404())
    $classes[] = 'sidebar';

  if (!get_option('show_avatars'))
    $classes[] = 'no-avatars';

  return $classes;
}
add_filter('body_class', 'twentythirteen_body_class');
/**
 * Adjust content_width value for video post formats and attachment templates.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_content_width() {
  global $content_width;

  if (is_attachment())
    $content_width = 724;
  elseif (has_post_format('audio'))
    $content_width = 484;
}
add_action('template_redirect', 'twentythirteen_content_width');
/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function twentythirteen_customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
  $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
  $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
}
add_action('customize_register', 'twentythirteen_customize_register');
/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JavaScript handlers to make the Customizer preview
 * reload changes asynchronously.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_customize_preview_js() {
  wp_enqueue_script('twentythirteen-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array('customize-preview'), '20141120', true);
}
add_action('customize_preview_init', 'twentythirteen_customize_preview_js');
function get_gl_images_path(){
  return get_template_directory_uri () . '/gl-assets/images/';
}
/**
 *
 * Print the hero markup for level 1 internal pages
 *
 * @param Integer $post_id get it from $post->ID
 */
function print_hero($post_id) {
  $hero_title = get_post_meta($post_id, 'hero_title', true);
  $hero_text = get_post_meta($post_id, 'hero_text', true);
  $hero_link = get_post_meta($post_id, 'hero_link', true);
  $hero_link_text = get_post_meta($post_id, 'hero_link_text', true);
  $hero_large_columns = get_post_meta($post_id, 'hero_content_width', true);
  $hero_medium_columns = ($hero_large_columns == 12) ? 12 : $hero_large_columns + 1;
  $hero_image_id = get_post_meta($post_id, 'hero_image', true);
  $hero_image = wp_get_attachment_url($hero_image_id);
  $hero_career_video = get_post_meta($post_id, 'video_link', true);
  $embedded_subscribe_url = get_post_meta($post_id, 'subscribe_url', true);
  ?>
  <div class="hero-wrapper">
    <div class="hero" style="background-image: url('<?php echo $hero_image ?>')">
      <div class="row">
        <div class="medium-<?php echo $hero_medium_columns ?> large-<?php echo $hero_large_columns ?> columns">
          <h1 class="hero-title">
            <?php echo $hero_title ?>
          </h1>
          <h3 class="hero-paragraph">
            <?php echo $hero_text ?>
          </h3>
          <?php
          if (is_array($hero_link)) {
            ?>
          <a href="<?php echo get_permalink($hero_link[0])?>#subnav" class="btn mod-white"><?php echo $hero_link_text?></a>
            <?php
          }
          ?>
		  <?php
          if ($embedded_subscribe_url){
            ?>
          <a href="<?php echo $embedded_subscribe_url;?>" class="btn mod-white" target="_blank"><?php echo $hero_link_text?></a>
            <?php
          }
          ?>
		<?php
             if (!empty($hero_career_video)) {
		  echo '<a href="'.$hero_career_video.'" rel="wp-video-lightbox" class="btn mod-white vp-a">'.$hero_link_text.'</a>';
             }
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
}
/**
 *
 * Print the subnav markup that goes below the top hero
 * on level 1 internal pages
 *
 * @param String $subnav_name refers to the wordpress menu name
 */
function print_subnav($subnav_name) {
  ?>
  <div class="subnav-wrapper">
    <div id="subnav" class="subnav-scroll-waypoint"></div>
    <nav class="subnav">
      <?php

      function subnav_add_classes_on_a($ulclass) {
        return preg_replace('/<a /', '<a class="subnav-list-link"', $ulclass);
      }

      add_filter('wp_nav_menu', 'subnav_add_classes_on_a');

      wp_nav_menu(array('menu' => $subnav_name, 'container' => false, 'theme_location' => 'primary_subnav', 'menu_class' => 'subnav-list', 'nav_menu_css_class' => 'subnav-list-item'));
      remove_filter('wp_nav_menu', 'subnav_add_classes_on_a');
      ?>
    </nav>
  </div>
  <?php
}
function get_news_type($category_base, $post_id) {
  $categories = wp_get_post_categories($post_id);
  foreach ($categories as $cat_id) {
    $cat = get_category($cat_id);
    if ($cat->category_parent === $category_base->term_id) {
      return $cat;
    }
  }
  return false;
}

function get_embedded_type($category_base, $post_id) {
  $categories = wp_get_post_categories($post_id);
  foreach ($categories as $cat_id) {
    $cat = get_category($cat_id);
    if ($cat->category_parent === $category_base->term_id) {
      return $cat;
    }
  }
  return false;
}

/*embedded*/


function print_embedded_card($post, $category_base, $show_image = true) {
  $type = get_embedded_type($category_base, $post->ID);
  $show_date = true;
   $thumb_img = get_post_meta($post->ID, 'thumbnail_image', true);
  if ($type) {
    $css_mod_type = 'mod-' . $type->slug;
    $category = get_embedded_type($type, $post->ID);
    $label = ($category) ? $category->name : $type->name;
	
    $show_date = ($type->slug == 'insights') ? false : true;
  } else {
    $label = $category_base->name;
  }
  $future_date=get_post_meta($post->ID, 'future_date', true);
			   $event_end_date=get_post_meta($post->ID, 'event_end_date', true);



                  $event_date = $future_date;
                  $today = date("Ymd");                  
                  if( $today <= $event_date && $label == "Event"){
                     $label = "next events";
                  } else if($today > $event_date && $label == "Event") {
                    $label = "<span style='color:#777;'>past events</span>";
                  } 


				/* if($future_date!=''){
					$date=mysql2date('j F Y', $future_date);
                }
                else {
					$date=get_the_date('F j, Y', $post->ID);
                } */
				if($future_date!='' && $event_end_date!=''){
					$date1=mysql2date('F j', $future_date);
					$date2=mysql2date('j, Y', $event_end_date);
					$date= $date1." - ".$date2;
                }elseif($future_date!='' && $event_end_date==''){
					$date=mysql2date('F j, Y', $future_date);
                }
                else {
					$date=get_the_date('F j, Y', $post->ID);
                  }


                  

  ?>

  <a class="news-card" href="<?php echo get_permalink($post->ID) ?>" data-equalizer-watch>
    <div class="news-card-header">
      <span class="news-card-type <?php echo $css_mod_type ?>"><?php echo $label; ?></span>
      

    </div>
  <?php
  
  $show_image = true;
  if ($show_image) {
    $thumb = wp_get_attachment_image( $thumb_img, 'medium' );
    echo "<span class='news-card-thumb'>".$thumb."</span>";
    ?>
      <!--<img class="news-card-thumb" src="<?php echo get_the_post_thumbnail_url($post);  ?>"/> -->
      
      <?php
    }
    ?>
    <h4 class="news-card-title">
    <?php echo $post->post_title ?>
    </h4>
    <?php
      if($show_date){
      ?>
        <span class="news-card-date"> <?php echo $date; ?></span>
        <?php
      }
      ?>
    <p class="news-card-text">
    <?php echo $post->post_excerpt ?>[...]
    </p>
  </a>
  
  <?php
}
/**
 *
 * Print the markup for the news card that goes on news lists
 *
 * @param wp_post $post
 * @param Boolean $show_image
 */
function print_news_card($post, $category_base, $show_image = true) {
  $type = get_news_type($category_base, $post->ID);
  $show_date = true;
   $thumb_img = get_post_meta($post->ID, 'thumbnail_image', true);
   $form = get_post_meta($post->ID, 'whitepaper_form', true);
  if ($type) {
    $css_mod_type = 'mod-' . $type->slug;
    $category = get_news_type($type, $post->ID);
    $label = ($category) ? $category->name : $type->name;
	
    $show_date = ($type->slug == 'insights') ? false : true;
  } else {
    $label = $category_base->name;
  }
  $future_date=get_post_meta($post->ID, 'future_date', true);
			   $event_end_date=get_post_meta($post->ID, 'event_end_date', true);



                  $event_date = $future_date;
                  $today = date("Ymd");                  
                  if( $today <= $event_date && $label == "Event"){
                     $label = "next events";
                  } else if($today > $event_date && $label == "Event") {
                    $label = "<span style='color:#777;'>past events</span>";
                  } 


				/* if($future_date!=''){
					$date=mysql2date('j F Y', $future_date);
                }
                else {
					$date=get_the_date('F j, Y', $post->ID);
                } */
				if($future_date!='' && $event_end_date!=''){
					$date1=mysql2date('F j', $future_date);
					$date2=mysql2date('j, Y', $event_end_date);
					$date= $date1." - ".$date2;
                }elseif($future_date!='' && $event_end_date==''){
					$date=mysql2date('F j, Y', $future_date);
                }
                else {
					$date=get_the_date('F j, Y', $post->ID);
                  }


                  

  ?>
 <?php if ( $label != 'White Papers' ) { ?>
  <a class="news-card" href="<?php echo get_permalink($post->ID) ?>" data-equalizer-watch>
    <div class="news-card-header">
      <span class="news-card-type <?php echo $css_mod_type ?>"><?php echo $label; ?></span>
      

    </div>
  <?php
  
  $show_image = true;
  if ($show_image) {
    $thumb = wp_get_attachment_image( $thumb_img, 'medium' );
    echo "<span class='news-card-thumb'>".$thumb."</span>";
    ?>
      <!--<img class="news-card-thumb" src="<?php echo get_the_post_thumbnail_url($post);  ?>"/> -->
      
      <?php
    }
    ?>
    <h4 class="news-card-title">
    <?php echo $post->post_title ?>
    </h4>
    <?php
      if($show_date){
      ?>
        <span class="news-card-date"> <?php echo $date; ?></span>
        <?php
      }
      ?>
    <p class="news-card-text">
    <?php echo $post->post_excerpt ?>[...]
    </p>
  </a>
  <?php } else{ ?>
  <a class="news-card" href="#" data-featherlight="#mylightbox" data-featherlight-iframe-height="500" style="" data-equalizer-watch>
	<div class="whiteiframe" id="mylightbox"><iframe src="https://go.pardot.com/l/283632/2017-06-22/3gjcl?Whitepaper_Link=<?php echo get_permalink($post->ID)?>&Whitepaper_Title=<?php echo $title; ?>" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe></div>
    <div class="news-card-header">
      <span class="news-card-type <?php echo $css_mod_type ?>"><?php echo $label; ?></span>   

    </div>
  <?php
  
  $show_image = true;
  if ($show_image) {
    $thumb = wp_get_attachment_image( $thumb_img, 'medium' );
    echo "<span class='news-card-thumb'>".$thumb."</span>";
    ?>
      <!--<img class="news-card-thumb" src="<?php echo get_the_post_thumbnail_url($post);  ?>"/> -->
      
      <?php
    }
    ?>
    <h4 class="news-card-title">
    <?php echo $post->post_title ?>
    </h4>
    <?php
      if($show_date){
      ?>
        <span class="news-card-date"> <?php echo $date; ?></span>
        <?php
      }
      ?>
    <p class="news-card-text">
    <?php echo $post->post_excerpt ?>[...]
    </p>
  </a>
  <?php } ?>
  <?php
}


function print_event_card($post) {

  $future_date=get_post_meta($post->ID, 'future_date', true);
         $event_end_date=get_post_meta($post->ID, 'event_end_date', true);



                  $event_date = $future_date;
                  $today = date("Ymd");                  
                  if( $today <= $event_date){
                     $label = "next events";
                  } else if($today > $event_date) {
                    $label = "<span style='color:#777;'>past events</span>";
                  } 


        /* if($future_date!=''){
          $date=mysql2date('j F Y', $future_date);
                }
                else {
          $date=get_the_date('F j, Y', $post->ID);
                } */
        if($future_date!='' && $event_end_date!=''){
          $date1=mysql2date('F j', $future_date);
          $date2=mysql2date('j, Y', $event_end_date);
          $date= $date1." - ".$date2;
                }elseif($future_date!='' && $event_end_date==''){
          $date=mysql2date('F j, Y', $future_date);
                }
                else {
          $date=get_the_date('F j, Y', $post->ID);
                  }


                  


 ?>
  <a class="news-card" href="<?php echo get_permalink($post->ID) ?>" data-equalizer-watch>
    <div class="news-card-header">
      <span class="news-card-type <?php echo $css_mod_type ?>"><?php echo $label; ?></span>
      

    </div>
  <?php
  
  $show_image = true;
  if ($show_image) {
    $thumb = wp_get_attachment_image( $thumb_img, 'medium' );
    echo "<span class='news-card-thumb'>".$thumb."</span>";
    ?>
      <!--<img class="news-card-thumb" src="<?php echo get_the_post_thumbnail_url($post);  ?>"/> -->
      
      <?php
    }
    ?>
    <h4 class="news-card-title">
    <?php echo $post->post_title ?>
    </h4>
   
   
        <span class="news-card-date"> <?php echo $date; ?></span>
     
    <p class="news-card-text">
    <?php echo $post->post_excerpt ?>[...]
    </p>
  </a>
  <?php 

}






















/**
 *
 * Print the markup for our work page
 *
 */
function print_our_work($our_work_post, $class) {
  $imgID  = get_post_thumbnail_id($our_work_post->ID);
  $imgAlt = get_post_meta($imgID,'_wp_attachment_image_alt', true);
  $images_path = get_template_directory_uri() . '/gl-assets/images/';
  echo '<li class="grid-item ' . $class . '">
      <a href="' . get_permalink($our_work_post->ID) . '#subnav">
        <div class="grid-item-image" style="background-image: url(\'' . wp_get_attachment_url(get_post_thumbnail_id($our_work_post->ID)) . '\')"></div>
        </a><div class="grid-item-hover"><a href="' . get_permalink($our_work_post->ID) . '#subnav">
          </a><div class="text-center"><a href="' . get_permalink($our_work_post->ID) . '#subnav">
            <h4 class="grid-item-title">' . $our_work_post->post_title . '</h4>
            <p class="grid-item-text">' . get_post_meta($our_work_post->ID, 'content_short_description', true) . '</p>
            </a><a href="' . get_permalink($our_work_post->ID) . '#subnav" class="btn mod-white">
              '.of_get_option('general_see_more_text').'
            </a>
          </div>
        </div>
        <img class="grid-item-placeholder" src="' . $images_path . 'grid-placeholder.gif" alt="'.$imgAlt.'">
    </li>';
}

/**
 *
 * Print the markup for half and half
 *
 */
function print_half_and_half( $what_we_do_post, $position, $read_more = TRUE ){
	$imgID  = get_post_thumbnail_id($what_we_do_post->ID);
 	$imgAlt = get_post_meta($imgID,'_wp_attachment_image_alt', true);
    $mod_class = ($position == 'left') ? 'mod-left' : 'mod-right';
    $column_mod_class = ($position == 'left') ? '' : 'medium-offset-6';
    $half_and_half = '
      <div class="half-half-wrapper '.$mod_class.'">
        <img class="half-half-image" src="'.wp_get_attachment_url(get_post_thumbnail_id($what_we_do_post->ID)).'" alt="'.$imgAlt.'">
        <div class="row">
          <div class="medium-6 columns '.$column_mod_class.'">
            <div class="half-half">
              <div class="half-half-label '.$what_we_do_post->number_label_color.'">
                '.$what_we_do_post->number_label.'
              </div>
              <a href="'.get_permalink($what_we_do_post->ID).'#subnav" class="half-half-action btn-read-more"><h2 class="half-half-title">
                '.$what_we_do_post->post_title.'
              </h2></a>
              <p class="half-half-text">
                '.get_post_meta($what_we_do_post->ID, 'content_short_description', true).'
              </p>';

        if( $read_more ) {
              $half_and_half .= '<a href="'.get_permalink($what_we_do_post->ID).'#subnav" class="half-half-action btn-read-more">'.of_get_option('general_read_more_text').' <i class="fa fa-angle-right"></i></a>';
        }

            $half_and_half .= '</div>
          </div>
        </div>
      </div>';

    echo $half_and_half;
}
// Filter to fix the Post Author Dropdown
add_filter('wp_dropdown_users', 'theme_post_author_override');
function theme_post_author_override($output){
  global $post, $user_ID;
  // return if this isn't the theme author override dropdown
  if (!preg_match('/post_author_override/', $output)) return $output;

  // return if we've already replaced the list (end recursion)
  if (preg_match ('/post_author_override_replaced/', $output)) return $output;

  // replacement call to wp_dropdown_users
  $output = wp_dropdown_users(array(
    'echo' => 0,
      'name' => 'post_author_override_replaced',
      'selected' => empty($post->ID) ? $user_ID : $post->post_author,
      'include_selected' => true
  ));

  // put the original name back
  $output = preg_replace('/post_author_override_replaced/', 'post_author_override', $output);

  return $output;
}
/* Return country range by IP */
function getIPCountry(){
  $ip = $_SERVER['REMOTE_ADDR'];
/*
  $server   = 'localhost'; // MySQL hostname
  $username = 'root'; // MySQL username
  $password = ''; // MySQL password
  $dbname   = 'ip_country'; // MySQL db name

  $remote_addr='16777218';
  //INET_ATON("'.$_SERVER['REMOTE_ADDR'].'");

  $db = mysql_connect($server, $username, $password) or die(mysql_error());
        mysql_select_db($dbname) or die(mysql_error());

  $sql = "SELECT * FROM country WHERE ip_from < $remote_addr < ip_to LIMIT 1";

  $row = mysql_fetch_row(mysql_query($sql));

  return $row[2];*/
  return "";
}
/* Return location by group (Latam, Au, North America, etc) */
function getLocation(){
  $country=getIPCountry();
  $countries['latam'] = array('AR','CL'); // define contries for Latam
  $countries['ua'] = array('UA'); // define AU for Ukranie
  $location=''; // empty is for global site;

  foreach ($countries as $key => $value) {
    if(in_array($country, $value)){
      $location=$key;
    }
  }
  return $location;
}
/* Redirect URL by Location */
function redirectUrlToCountry(){
  $location=getLocation();
  switch ($location) {
    case 'latam':
      header("Location: /latam");
      exit;
    case 'ua':
      header("Location: /ua");
      exit;
    default:
      break;
  }
}

// get post ids for s parameter search
function getPostIds($data){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
$post_ids = array_unique(array_merge( $first_ids, $second_ids));	

//print_r($post_ids);exit;
return $post_ids;	
	
}

// get post ids by country for s parameter search
function getPostIdsbyCountry($data){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
									'relation' => 'OR',
									array(
										'taxonomy' => 'offices',
										'field'    => 'name',
										'terms'    => 'Ukraine',
									)
								),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
									'relation' => 'OR',
									array(
										'taxonomy' => 'offices',
										'field'    => 'name',
										'terms'    => 'Ukraine',
									)
								),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
$post_ids = array_unique(array_merge( $first_ids, $second_ids));	
return $post_ids;	
	
}


// get post ids by country for s parameter search
function getPostIdsbyCountries($data,$country){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
						),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$first_ids1 = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
						),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$second_ids1 =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	//print_r($first_ids);print_r($first_ids1);echo "yes";print_r($second_ids);echo "yes1";print_r($second_ids1);die;
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
$post_ids = array_unique(array_merge( $first_ids1, $post_ids));
$post_ids = array_unique(array_merge( $second_ids1, $post_ids));		
return $post_ids;	
	
}

// get post ids by country for s parameter search
function getPostIdsbyJobCat($data,$job_category){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
						),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$first_ids1 = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
						),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$second_ids1 =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	//print_r($first_ids);print_r($first_ids1);echo "yes";print_r($second_ids);echo "yes1";print_r($second_ids1);die;
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
$post_ids = array_unique(array_merge( $first_ids1, $post_ids));
$post_ids = array_unique(array_merge( $second_ids1, $post_ids));	
//print_r($post_ids);	
return $post_ids;	
	
}



// get post ids by country and jobcategory and city for s parameter search
function getPostIdsbyJobcatCountryCity($data,$job_category,$office,$country){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'job_categories',
									'field'    => 'name',
									'terms'    => array( $job_category ),
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),
							),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$first_ids1 = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'job_categories',
									'field'    => 'name',
									'terms'    => array( $job_category ),
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),								
								array(
									'taxonomy' => 'job_skills',
									'field'    => 'name',
									'terms'    => $data,
								),
							),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'job_categories',
									'field'    => 'name',
									'terms'    => array( $job_category ),
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),
							),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$second_ids1 =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
								'relation' => 'AND',	
								array(
									'taxonomy' => 'job_categories',
									'field'    => 'name',
									'terms'    => array( $job_category ),
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),							
								array(
									'taxonomy' => 'job_skills',
									'field'    => 'name',
									'terms'    => $data,
								),
							),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
$post_ids = array_unique(array_merge( $first_ids1, $post_ids));
$post_ids = array_unique(array_merge( $second_ids1, $post_ids));	
return $post_ids;	
	
}

// get post ids by country and jobcategory for s parameter search
function getPostIdsbyJobCatCountry($data,$country, $job_category){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
						),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$first_ids1 = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
							),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$second_ids1 =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),							
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => $data,
							),
						),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	//print_r($first_ids);print_r($first_ids1);echo "yes";print_r($second_ids);echo "yes1";print_r($second_ids1);die;
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
$post_ids = array_unique(array_merge( $first_ids1, $post_ids));
$post_ids = array_unique(array_merge( $second_ids1, $post_ids));				
return $post_ids;	
	
}

// get post ids by country for s parameter search
function getPostIdsbyCity($data,$office,$country){
	
	$first_ids = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $office,
						),
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $country,
						),
					),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$first_ids1 = get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		// 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 's'=> $data,
		 'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $office,
						),
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $country,
						),
						array(
							'taxonomy' => 'job_skills',
							'field'    => 'name',
							'terms'    => $data,
						),
					),
		// 'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	
$sub = array('relation' => 'OR');

$sub[] = array(
    'key'     => 'job_description',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'we_offer',
    'value'   => $data,
    'compare' => 'LIKE',
);

$sub[] = array(
    'key'     => 'job_type',
    'value'   => $data,
    'compare' => 'LIKE',
);

$second_ids =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $office,
						),
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $country,
						),
					),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
	
	$second_ids1 =  get_posts( array(
	     'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  -1,
		 'order_by' => 'post_date',
		 'meta_query'=> $sub,
		 'tax_query' => array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $office,
						),
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => $country,
						),
						array(
							'taxonomy' => 'job_skills',
							'field'    => 'name',
							'terms'    => $data,
						),
					),
		 //'paged' => $paged,	 
	     'fields'        => 'ids'
	));
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
$post_ids = array_unique(array_merge( $first_ids1, $post_ids));
$post_ids = array_unique(array_merge( $second_ids1, $post_ids));		
return $post_ids;	
	
}


//get multilingual dynamic date
function get_dynamic_date($date){
	$time=strtotime($date);
   $month=date("M",$time);
   $day=date("j",$time);
   $year=date("Y",$time);
  $d = date_parse_from_format("M j, Y", $date);
  $dyn_date;
  switch ($month){
	  case 'Jan':
	  $dyn_mnth =  of_get_option('jan_text');
	  break;
	  case 'Feb':
	  $dyn_mnth =  of_get_option('feb_text');
	  break;
	  case 'Mar':
	  $dyn_mnth =  of_get_option('mar_text');
	  break;
	  case 'Apr':
	  $dyn_mnth =  of_get_option('apr_text');
	  break;
	  case 'May':
	  $dyn_mnth =  of_get_option('may_text');
	  break;
	  case 'Jun':
	  $dyn_mnth =  of_get_option('jun_text');
	  break;
	  case 'Jul':
	  $dyn_mnth =  of_get_option('jul_text');
	  break;
	  case 'Aug':
	  $dyn_mnth =  of_get_option('aug_text');
	  break;
	  case 'Sep':
	  $dyn_mnth =  of_get_option('sep_text');
	  break;
	  case 'Oct':
	  $dyn_mnth =  of_get_option('oct_text');
	  break;
	  case 'Nov':
	  $dyn_mnth =  of_get_option('nov_text');
	  break;
	  case 'Dec':
	  $dyn_mnth =  of_get_option('dec_text');
	  break;
  }
  $dyn_date = $day." ".$dyn_mnth." ".$year;
  return $dyn_date;
}

/*function disable_new_posts() {

$selfurl = $_SERVER['PHP_SELF'];
$post_type = $_GET['post_type'];
$siteurl= site_url();

 if( $siteurl != "https://gl1.globallogic.com")
{
	if($selfurl=="/wp-admin/post-new.php" && $post_type=="gl_job")
	{
		wp_redirect(admin_url() );
	}
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'gl_job') {
        echo '<style type="text/css">a.page-title-action{ display:none; }</style>';
    }
	$post_type = get_post_type($_GET['post']);
	if ($post_type == 'gl_job') {
		echo '<style type="text/css">a.page-title-action{ display:none; }</style>';
	}

	remove_submenu_page( 'edit.php?post_type=gl_job', 'post-new.php?post_type=gl_job' );
}

}
add_action('admin_menu', 'disable_new_posts'); */

//removes quick edit from custom post type list
function remove_quick_edit( $actions ) {
	global $post;
    if( $post->post_type == 'gl_job' ) {
		unset($actions['inline hide-if-no-js']);
	}
    return $actions;
}

if (is_admin()) {
	add_filter('post_row_actions','remove_quick_edit',10,2);
}
?>
<?php

$site_url = get_site_url();

		if($site_url == "https://gl1.globallogic.com/il" || $site_url == "gl1.globallogic.com/il" ||  $site_url == "https://gl1.globallogic.com/pl" || $site_url == "gl1.globallogic.com/pl" || $site_url == "https://gl1.globallogic.com/ua" || $site_url == "gl1.globallogic.com/ua" || $site_url == "https://gl1.globallogic.com/latam" ||$site_url == "gl1.globallogic.com/latam"){
function wpse28782_remove_menu_items() {
        remove_menu_page( 'edit.php?post_type=gl_job' );
}
add_action( 'admin_menu', 'wpse28782_remove_menu_items' );
}



if($site_url == "https://gl1.globallogic.com" || $site_url == "gl1.globallogic.com" ){
function wpse28782_remove_menu_items1() {
	$current_user = wp_get_current_user();
    if( !in_array('administrator', (array)$current_user ->roles ) ) :
		?>
		<script>
		jQuery(document).ready(function($){
			$('a[href$="edit-tags.php?taxonomy=offices&post_type=gl_job"]').hide();
			$('#offices-adder a').hide();
		});
		</script>
		<?php
		endif;
}
add_action( 'admin_footer', 'wpse28782_remove_menu_items1' );
}
?>


<?php
add_action( 'init', 'create_work_tax',0 );

function create_work_tax() {
  register_taxonomy( 
    'work-category',
    'our-work',
    array(
      'label' => __( 'Category' ),
      'hierarchical' => true,
    )
  );
}
?>

<?php
add_action( 'init', 'create_embedded_tax',0 );

function create_embedded_tax() {
  register_taxonomy( 
    'embedded-category',
    'gl_news',
    array(
      'label' => __( 'Embedded Category' ),
      'hierarchical' => true,
    )
  );
}
?>

<?php
add_action( 'init', 'create_what_we_do_tax',0 );

function create_what_we_do_tax() {
  register_taxonomy( 
    'work-with-us-category',
    'what-we-do',
    array(
      'label' => __( 'Category' ),
      'hierarchical' => true,
    )
  );
}





/**
 *
 * Print the markup for the news card that goes on news lists
 *
 * @param wp_post $post
 * @param Boolean $show_image
 */
function print_news_card1($post, $category_base, $show_image = true) {
  $type = get_news_type($category_base, $post->ID);
  $show_date = true;
   $thumb_img = get_post_meta($post->ID, 'thumbnail_image', true);
  if ($type) {
    $css_mod_type = 'mod-' . $type->slug;
    $category = get_news_type($type, $post->ID);
    $label = ($category) ? $category->name : $type->name;
    
  }
  
  ?>

   <div class="column">
			<a class="news-card" href="<?php echo get_permalink($post->ID) ?>" data-equalizer-watch style="height: 438px;">    
			<h4 class="news-card-title">[ News ]</h4>
      <img style="width:100%;" src="<?php echo get_home_url(); ?>/wp-content/themes/gltheme/gl-assets/images/home/news.jpg"
      class="attachment-medium size-medium" alt="news"  sizes="(max-width: 300px) 100vw, 300px">
    
    <h4>
    <?php echo $post->post_title ?>
    </h4>
    <p class="news-card-text">
    <?php echo $post->post_excerpt ?>[...]
    </p>
     </a>  
		</div>
  
  <?php
}


/*

// Do something with the data entered 
add_action( 'save_post', 'myplugin_save_postdata' );

// When the post is saved, saves our custom data 
function myplugin_save_postdata( $post_id ) {

 // First we need to check if the current user is authorised to do this action. 
 if ( 'page' == $_POST['gl_career'] ) {
    if ( ! current_user_can( 'edit_page', $post_id ) )
       return;
 } else {
   if ( ! current_user_can( 'edit_post', $post_id ) )
       return;
 }

 $mydata = "IRC".$post_id; // Do something with $mydata 
 
 $mydata1 = " (IRC".$post_id.")"; // Do something with $mydata1 

 update_post_meta( $post_id, 'job_vacancy_number', $mydata );
 
 update_post_meta( $post_id, 'title', $mydata1 );
}

*/


/*
function disable_acf_load_field( $field ) {

$field['disabled'] = 1;
return $field;

}
add_filter('acf/load_field/name=acf-field-job_vacancy_number', 'disable_acf_load_field');
*/

function wp_infinitepaginate(){ 
    $loopFile        = $_POST['loop_file'];
    $paged           = $_POST['page_no'];
    $posts_per_page  = get_option('posts_per_page');
 
    # Load the posts
    query_posts(array('paged' => $paged )); 
    get_template_part( $loopFile );
 
    exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate'); // if user not logged in













// add the ajax fetch js
add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
function fetchCareer(){
	
	$("#loadcounter").val('2');
	jQuery('.main_career_pg_job_list_section').html('<div align=center style="color:#ff6600">Loading... <img src=<?php echo esc_url( get_template_directory_uri() ); ?>/images/ajax-loader.gif /></div>');
    jQuery.ajax({
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        type: 'post',
        data: { action: 'data_fetch', careerKeywords: jQuery('#by_keyword').tagEditor('getTags')[0].tags, job_category: jQuery('#job_category').val(), country: jQuery('#country').val(), office: jQuery('#office').val() },
        success: function(data) {
            jQuery('.main_career_pg_job_list_section').html( data );
        }
    });

}
</script>

<?php
}


// the ajax function
add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){


	        	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				
	        	$args = array(
				 'post_type' => 'gl_career',
				 'post_status' => 'publish',
				 'posts_per_page' => 6,
				 'order_by' => 'post_date',
				 'paged' => $paged 
				);
				
				
		$job_category = filter_input(INPUT_POST, 'job_category', FILTER_SANITIZE_STRING);
		$office = filter_input(INPUT_POST, 'office', FILTER_SANITIZE_STRING);
    	$country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
		$careerKeywords = $_REQUEST['careerKeywords'];
    
		if( $job_category != 'all' && ( $office != 'all' || $country != 'all' ) && $careerKeywords=="")
		{
			// Ok, entonces necesitamos cruzar las taxonomias: tax_query en los argumentos
			
			/* Veamos si el pais o la oficina tienen protagonismo */
			
			$offices = array();
			
			if( $office == 'all' )
			{
				$offices[] = $country;
			}else
			{
				$offices[] = $office;
			}
			
			$args['tax_query'] = array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'offices',
					'field'    => 'name',
					'terms'    => $offices,
				),
				array(
					'taxonomy' => 'job_categories',
					'field'    => 'name',
					'terms'    => array( $job_category ),
				),
			);
		}
		elseif( $job_category == 'all' && ( $office != 'all' || $country != 'all' ) && $careerKeywords=="")
		{
			// Bien, entonces podemos olvidarnos de momento de la categoria de los jobs
			
			$offices = array();
			
			if( $office == 'all' )
			{
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'offices',
						'field'    => 'name',
						'terms'    => $country,
					),
				);
				
			} else
			{
				$offices = $office;				
				
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'offices',
						'field'    => 'name',
						'terms'    => $offices,
					),
				);
			}
			
		}
		elseif( $job_category != 'all' && ( $office == 'all' && $country == 'all' ) && $careerKeywords=="")
		{
			// Bien, entonces solo prestaremos atencion a la categoria de jobs, van a ver los jobs de todo el mundo, pero solo de una categoria
			
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'job_categories',
					'field'    => 'name',
					'terms'    => array( $job_category ),
				),
			);
		}
		elseif( $job_category == 'all' && $office == 'all' && $country == 'all' && $careerKeywords=="")
		{
			// Aqui no hay mucho que hacer, el usuario quiere ver todo, pero dejamos la condicion 
		}
		elseif($careerKeywords!=""){			
			
			if($job_category != 'all' &&  $office != 'all' && $country != 'all' ){
				
				/*
				$args['tax_query'] = array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'job_categories',
						'field'    => 'name',
						'terms'    => array( $job_category ),
					),
					array(
						'taxonomy' => 'offices',
						'field'    => 'name',
						'terms'    => $office,
					),
					array(
						'taxonomy' => 'offices',
						'field'    => 'name',
						'terms'    => $country,
					),
					array(
						'taxonomy' => 'job_skills',
						'field'    => 'name',
						'terms'    => array_values($careerKeywords),
					),
				);
				 */
				 
				 $locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'job_categories',
									'field'    => 'name',
									'terms'    => array( $job_category ),
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),
								array(
									'taxonomy' => 'job_skills',
									'field'    => 'name',
									'terms'    => array_values($careerKeywords),
								),
							),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIdsbyJobcatCountryCity($value,$job_category,$office,$country)));
					  }
					
					
					//print_r(array_unique(array_merge($postIds,$locSkids))); die();
					
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
					
					//$args['post__in']  = array_unique(array_merge($postIds,$locSkids));
				
				
				
			}			
			elseif($office != 'all'){
				
				$locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
								'relation' => 'AND',
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $office,
								),
								array(
									'taxonomy' => 'offices',
									'field'    => 'name',
									'terms'    => $country,
								),
								array(
									'taxonomy' => 'job_skills',
									'field'    => 'name',
									'terms'    => array_values($careerKeywords),
								),
							),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIdsbyCity($value,$office,$country)));
					  }
					
					
					//print_r(array_unique(array_merge($postIds,$locSkids))); die();
					
					//$args['post__in']  = array_unique(array_merge($postIds,$locSkids));
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
				
				
			}
			elseif($country != 'all'){
					if($job_category != 'all'){
						
						
						/*
						$args['tax_query'] = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => array_values($careerKeywords),
							),
						);
						*/
						
						
						$locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => array_values($careerKeywords),
							),
						),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIdsbyJobCatCountry($value,$country,$job_category)));
					  }
					
					
					//print_r($postIds); die();
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
					
					
						
						
						
						
						
						
						
						
					}
					else{
						
						
						/*
						$args['tax_query'] = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => array_values($careerKeywords),
							),
						);
						*/
						
						$locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'offices',
								'field'    => 'name',
								'terms'    => $country,
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => array_values($careerKeywords),
							),
						),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIdsbyCountries($value,$country)));
					  }
					
					
					//print_r($postIds); die();
					
					//$args['post__in']  = array_unique(array_merge($postIds,$locSkids));
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
						
						
						
						
						
					}					
			}
			elseif($job_category != 'all'){
				$locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'job_categories',
								'field'    => 'name',
								'terms'    => array( $job_category ),
							),
							array(
								'taxonomy' => 'job_skills',
								'field'    => 'name',
								'terms'    => array_values($careerKeywords),
							),
						),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIdsbyJobCat($value,$job_category)));
					  }
					
					
					//print_r($postIds); die();
					
					//$args['post__in']  = array_unique(array_merge($postIds,$locSkids));
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
				
				
			}
			else{
				
				//echo implode(" ",array_values($careerKeywords));exit;
				/*$args['tax_query'] = array(
						'relation' => 'OR',
						array(
							'taxonomy' => 'offices',
							'field'    => 'name',
							'terms'    => array_values($careerKeywords),
						),
						array(
							'taxonomy' => 'job_skills',
							'field'    => 'name',
							'terms'    => array_values($careerKeywords),
						),
					);
					*/
					$locSkids = get_posts( 
					array(
						 'post_type' => 'gl_career',
						 'post_status' => 'publish',
						 //'posts_per_page' => -1,
						 'order_by' => 'post_date',
						 //'paged' => $paged,
						 'tax_query' => array(
									'relation' => 'OR',
									array(
										'taxonomy' => 'offices',
										'field'    => 'name',
										'terms'    => array_values($careerKeywords),
									),
									array(
										'taxonomy' => 'job_skills',
										'field'    => 'name',
										'terms'    => array_values($careerKeywords),
									),
								),
						 'fields' => 'ids'
						)
					 );
					
					$postIds = array();
					foreach (array_values($careerKeywords) as $key => $value) {
					    $postIds = array_unique(array_merge($postIds,getPostIds($value)));
					  }
					
					
					//print_r($postIds); die();
					
					//$args['post__in']  = array_unique(array_merge($postIds,$locSkids));
					$rposts = array_unique(array_merge($postIds,$locSkids));
					if (empty($rposts)) {
						$args['post__in']  = array(0);
					}
					else{
						$args['post__in']  = $rposts;
					}
										
					
					//print_r($postIds); exit;
					
					//$args['post__in']  = array('12484','12501');
			}	
			
			/*$args = array(
				 'post_type' => 'gl_career',
				 'post_status' => 'publish',
				 'posts_per_page' =>  6,
				 'order_by' => 'post_date',
				 's'=> implode(" ",array_values($careerKeywords)),
				 'paged' => $paged 
				);*/
				
				/*$args = array(
				 'post_type' => 'gl_career',
				 'post_status' => 'publish',
				 'posts_per_page' =>  6,
				 'order_by' => 'post_date',
				 's'=> implode(" ",array_values($careerKeywords)),
				 'paged' => $paged 
				);*/
				
				
				
				
				/*
		$args = array(
         'post_type' => 'gl_career',
		 'post_status' => 'publish',
		 'posts_per_page' =>  6,
		 'order_by' => 'post_date',
		 'paged' => $paged,
        'relation' => 'OR',
        'meta_query' => array(
                array(
                        'key' => 'post_title',
                        'value' => 'developer' ,
                        'compare' => 'LIKE'
                ),
                array(
                        'key' => 'post_title',
                        'value' => 'android' ,
                        'compare' => 'LIKE'
                )
        )
);*/

/*
// first query
$first_ids = get_posts( array(
    'post_type' => 'gl_career',
	 'post_status' => 'publish',
	 'posts_per_page' =>  -1,
	 'order_by' => 'post_date',
	 's'=> 'developer',
	 'paged' => $paged,	 
     'fields'        => 'ids'
));

// second query
$second_ids = get_posts( array(
    'post_type' => 'gl_career',
	 'post_status' => 'publish',
	 'posts_per_page' =>  -1,
	 'order_by' => 'post_date',
	 's'=> 'android',
	 'paged' => $paged,	 
     'fields'        => 'ids'
));

// merging ids
$post_ids = array_unique(array_merge( $first_ids, $second_ids));
print_r($post_ids);die();


				
*/

		}


/*


		
		if( $job_category == 'all' && $office == 'all' && $country == 'all' && $careerKeywords=="")
		{
			
		}
		elseif($job_category!="all"){
			if($office == 'all' && $country == 'all' && $careerKeywords==""){
				
			}
			
			
		}
		elseif($office!="all"){
			
		}
		elseif($country!="all"){
			
		}
		elseif($careerKeywords!=""){
			
		}
		elseif($job_category != 'all' && $office != 'all' && $country != 'all' && $careerKeywords!=""){			
						
			$args['tax_query'] = array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'offices',
					'field'    => 'name',
					'terms'    => $office,
				),array(
					'taxonomy' => 'offices',
					'field'    => 'name',
					'terms'    => $country,
				),
				array(
					'taxonomy' => 'job_categories',
					'field'    => 'name',
					'terms'    => array( $job_category ),
				),
			);
		}
		
				
				*/
				//print_r($args);//die();
				
	        	$query = new WP_Query( $args );
				//echo $wpdb->last_query;//lists only single query
				//echo $wpdb->last_error;
				$posts = array();
				
				if ( $query->have_posts() ) :
				while( $query->have_posts() ) : $query->the_post(); 
				$job_vacancy_number2 = get_field('job_vacancy_number', get_the_ID());
				if(!$job_vacancy_number2)
					$job_vacancy_number2 = get_post_meta( get_the_ID(), 'acf-field-job_vacancy_number', true );
				?>
	            <div class="career_pg_list_of_job_order">
	            	<?php 
	            	
	            	$myvals = get_post_meta(get_the_ID());
					//print_r($myvals); 
	            	?>
	                <a href="<?php echo get_permalink() ?>">
	                    <span class="career_pg_job_name">
	                        <?php echo get_the_title(); ?> (<?php echo $job_vacancy_number2;  ?>)
	                    </span>
	                    <span class="career_pg_job_location">
	                        <?php 
								$term_list = wp_get_post_terms( get_the_ID(), 'offices', array( "fields" => "all", "orderby" => "parent", "order" => "ASC" ) );
								  //echo "<!--";
								  //var_dump($term_list);
								  //echo " -->";
								  if( count( $term_list ) > 2 ){
									echo $term_list[0]->name . ', ' . __( 'Multiple cities', 'gl');
								  } else
								    {
									
									  $l = 0;
									  foreach( $term_list as $location ){
										if( $l == 0 )
										{
											echo $location->name;
										} else
										  {
											echo ', ' . $location->name;
										}
										$l++;
									  }
								  }
							  ?>
	                    </span>
	                    <span class="career_pg_job_posted_date">
	                        <?php
								$lastmodified = get_the_modified_time('U');
								$posted = get_the_time('U');
								echo human_time_diff($posted,current_time( 'U' )). " ago";
							?>                    
	                    </span>
	                </a>
	            </div>
	           <?php endwhile;
	
				wp_reset_query();
				$totpag = $query->max_num_pages;
				if($paged >= $totpag){}
				else{
				?>
				
	            <div class="career_pg_load_more_btn">
	               <div class="career_pg_load_more_btn_main">Load More</div>
	               <a class="inifiniteLoader" style="display: none;">Loading... <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ajax-loader.gif" /></a>
	            </div>  
	              
	            <?php 
				}
	             else:
					 ?>
					 
					 
					 <div class="jobs-grid-wrapper">
    <div class="down-arrow-triangle"></div>
    <div class="row">
      <div class="large-11 large-centered columns">
        <div class="row medium-up-2 jobs-grid"></div>
      </div>
    </div>
    <div class="row show-for-medium">
      <div class="large-10 large-centered columns text-center">
       
				
        <p class="find-jobs-message"><span style="font-weight: bold; font-size: 0.985rem; display: inline;">We appreciate your interest in GlobalLogic.</span><br>As of now, we do not have any job matching your search criteria. Please visit again or submit your resume at <a href="mailto:info@globallogic.com">info@globallogic.com</a></p>
		      </div>
    </div>
  </div>
					
					 
					 
				<!--	 
	             <div class="career_pg_tq_message record_not_fnd">		            
		              <div class="inner_career_pg_tq_message">
		              <h1 class="orbit_title">We appreciate your interest in GlobalLogic.</h1>
		              <p>
		                As of now, we do not have any job matching your search criteria.<br>
		                Please visit again or submit your resume at <a href="mailto:info@globallogic.com">info@globallogic.com</a>
		              <p>
		            </div>
		        </div>  
		        -->
		              
				<?php
				endif;
				?>
	
	<?php

    die();
}

function my_wpcf7_validate_text( $result, $tag ) {

   $type = $tag['type'];
   $name = $tag['name'];
   $value = $_POST[$name];

   if ( 'name' == $name ){
   $regex = '/^[a-zA-Z. ]*$/';
   $Valid = preg_match($regex,  $value, $matches );
           if ( $Valid > 0 ) {
           }
           else {
               $result->invalidate( $tag, wpcf7_get_message( 'invalid_name' ) );
           }
       }

   if ( 'firstname' == $name ){
   $regex = '/^[a-zA-Z. ]*$/';
   $Valid = preg_match($regex,  $value, $matches );
           if ( $Valid > 0 ) {
           }
           else {
               $result->invalidate( $tag, wpcf7_get_message( 'invalid_firstname' ) );
           }
       }
   
   if ( 'lastname' == $name ){
   $regex = '/^[a-zA-Z. ]*$/';
   $Valid = preg_match($regex,  $value, $matches );
           if ( $Valid > 0 ) {
           }
           else {
               $result->invalidate( $tag, wpcf7_get_message( 'invalid_lastname' ) );
           }
       }
   
   if ( 'referName' == $name ){
   $regex = '/^[a-zA-Z. ]*$/';
   $Valid = preg_match($regex,  $value, $matches );
           if ( $Valid > 0 ) {
           }
           else {
               $result->invalidate( $tag, wpcf7_get_message( 'invalid_name' ) );
           }
       }
   
   
   return $result;
}
add_filter( 'wpcf7_validate_text*', 'my_wpcf7_validate_text' , 10, 2 );

add_filter( 'wpcf7_messages', 'mywpcf7_text_messages' );
function mywpcf7_text_messages( $messages ) {
   return array_merge( $messages, array(
                                        'invalid_name' => array(
                                                                'description' => __( "Name can contain alphabets and the following special characters .", 'contact-form-7' ),
                                                                'default' => __( 'Name can contain alphabets and the following special characters .', 'contact-form-7' )
                                                                ),
                                        'invalid_firstname' => array(
                                                                'description' => __( "First Name can contain alphabets and the following special characters .", 'contact-form-7' ),
                                                                'default' => __( 'FirstName can contain alphabets and the following special characters .', 'contact-form-7' )
                                                                ),
                                       'invalid_lastname' => array(
                                                                'description' => __( "Last Name can contain alphabets and the following special characters .", 'contact-form-7' ),
                                                                'default' => __( 'Last Name can contain alphabets and the following special characters .', 'contact-form-7' )
                                                                ),
																));
}

// Custome URL START 
function custom_rewrite_tag() {
 add_rewrite_tag('%JobID%', '([^&]+)');
 add_rewrite_tag('%refID%', '([^&]+)');
 add_rewrite_tag('%apiName%', '([^&]+)');
 add_rewrite_tag('%IRCno%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);


function custom_rewrite_rule() {
	
	$cUrls = $_SERVER['REQUEST_URI'];
	
	// For Referral URL	   
	add_rewrite_rule('^gl_career/([^/]*)/([^/]*)/([^/]*)/?','index.php?gl_career=$matches[1]&JobID=$matches[2]&refID=$matches[3]','top');
	
	// For Webservices URL	
	if (strpos($cUrls,'/latam/') !== false) {
		add_rewrite_rule('^webservices/([^/]*)/?','index.php?page_id=10481&apiName=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/ua/') !== false) {
		add_rewrite_rule('^webservices/([^/]*)/?','index.php?page_id=11813&apiName=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/il/') !== false) {
		add_rewrite_rule('^webservices/([^/]*)/?','index.php?page_id=10415&apiName=$matches[1]','top');
	}
	else{
		add_rewrite_rule('^webservices/([^/]*)/?','index.php?page_id=12532&apiName=$matches[1]','top');
	}
	
	// For Similar-career URL
	if (strpos($cUrls,'/latam/') !== false) {
		add_rewrite_rule('^similar-career/([^/]*)/?','index.php?page_id=10349&IRCno=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/ua/') !== false) {
		add_rewrite_rule('^similar-career/([^/]*)/?','index.php?page_id=11630&IRCno=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/il/') !== false) {
		add_rewrite_rule('^similar-career/([^/]*)/?','index.php?page_id=10289&IRCno=$matches[1]','top');
	}
	else{
		add_rewrite_rule('^similar-career/([^/]*)/?','index.php?page_id=12493&IRCno=$matches[1]','top');
	}
	
	
	// For thank-you-career URL
	if (strpos($cUrls,'/latam/') !== false) {
		add_rewrite_rule('^thank-you-career/([^/]*)/?','index.php?page_id=10346&IRCno=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/ua/') !== false) {
		add_rewrite_rule('^thank-you-career/([^/]*)/?','index.php?page_id=11632&IRCno=$matches[1]','top');
	}
	elseif(strpos($cUrls,'/il/') !== false) {
		add_rewrite_rule('^thank-you-career/([^/]*)/?','index.php?page_id=10287&IRCno=$matches[1]','top');
	}
	else{
		add_rewrite_rule('^thank-you-career/([^/]*)/?','index.php?page_id=12455&IRCno=$matches[1]','top');
	}
 }
 add_action('init', 'custom_rewrite_rule', 10, 0);
// Custome URL END
/*

add_action( 'wpcf7_mail_sent', 'handle_form_submission' ); 

function handle_form_submission( $contact_form ) {
    $title = $contact_form->title;
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
        // handle the data here e.g. submit to CRM
        
        print_r($posted_data);
    }
}

*/


add_action("wpcf7_before_send_mail", "wpcf7_do_something");

function wpcf7_do_something($WPCF7_ContactForm)
{
	// For apply now job submit form
    if ( 12415 == $WPCF7_ContactForm->id() || 10397 == $WPCF7_ContactForm->id() || 11795 == $WPCF7_ContactForm->id() || 10458 == $WPCF7_ContactForm->id() ) {

        //Get current form
        $wpcf7      = WPCF7_ContactForm::get_current();
		
		// get current SUBMISSION instance
        $submission = WPCF7_Submission::get_instance();

        // Ok go forward
        if ($submission) {

            // get submission data
            $data = $submission->get_posted_data();
			 $data['uploaded_files'] = $submission->uploaded_files();
			//var_dump($data);
			//die;
            // nothing's here... do nothing...
            if (empty($data))
                return;
			
			
			
			//$tmpfile = "https://gl1.globallogic.com".$data['uploaded_files']['resume'];
			$tmpfile = $data['uploaded_files']['resume'];
			
			$path_parts = pathinfo($data['resume']);
		    $filename = $path_parts['basename']; 
		    $filetype = $path_parts['extension'];
			
			//die;
			
			//$localFile = $data['uploaded_files']['resume'];
//$fp = fopen($localFile, 'r');

//$curl = curl_init();

//$cfile = new CURLFILE($data['uploaded_files']['resume'], $filetype, $filename);
			
			
			
			
			
			
			
			
			//$tmpfile = $data['resume']['tmp_name'];
		   // $filename = basename($data['resume']['name']); 
		   // $filetype = $data['resume']['type'];
			
			//print($filename);die;
			$data1 = array(
			'first_name'=>$data['firstname'],
			'last_name'=>$data['lastname'],
			"email"=>$data['email'],
			"mobile"=>$data['phoneNumber'],
			"resume"=>curl_file_create($tmpfile, mime_content_type($filetype), $filename),
			"location"=>$data['mailLoc'],
			"linkedin"=>$data['linkedin'],
			"cover_letter"=>$data['coverLetter'],
			"irc_number"=>$data['mailIRC'],
			);
			
			if(isset($data['referralId']) && $data['referralId']!="")
				$data1['refid ']=$data['referralId'];
			 
			
			/*
			$data1 = array(			
			"file"=>curl_file_create($tmpfile, $filetype, $filename),
			);*/
	
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "https://glo-staging.globallogic.com/gloapis/v1/glwebsite/candidates");
			//curl_setopt($curl, CURLOPT_URL, "http://s369512520.onlinehome.us/KSWN/index.php/webservices/upload");
			curl_setopt($curl,CURLOPT_USERPWD,"avohi.gl:Roman@321##");
			//curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");		
			curl_setopt($curl, CURLOPT_POST, true);
			//$headers = array("content-type: multipart/form-data","content-type: application/pdf","content-type: application/x-www-form-urlencoded"); // cURL headers for file uploading
			$headers = array("content-type: multipart/form-data"); // cURL headers for file uploading
			//print_r($headers);die;
			//curl_setopt($curl, CURLOPT_HEADER, 1);
			//curl_setopt($curl, CURLINFO_HEADER_OUT, true);
			//curl_setopt($curl, CURLOPT_HEADER, true);
			//curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpfile));
			
			/*$headers = array();
			$headers[] = "Accept: application/json";
			
				*/			
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			//curl_setopt($curl_handle, CURLOPT_POST, 1);
			
			//curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data1));			
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data1);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$result22 = curl_exec($curl);
			if (curl_errno($curl))
			{
				//print "Error: " . curl_error($curl);die;
			}
			else{				
				//var_dump($result22);die;
				
				
				$resultApi = json_decode($result22);
				//echo $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				//print_r($resultApi);
				if(isset($resultApi->error)){
					$resultApi1 = json_decode(json_encode($resultApi->error),true);
					$apiMessage = $resultApi1['message'];
					$apiReason = $resultApi1['reason'];
					$_SESSION['apiError'] = "";
					$_SESSION['apiError'] = $apiMessage."<br>".$apiReason;
				 			 
				add_filter("wpcf7_ajax_json_echo", function ($response, $result, $apiError) {
				
				
				   $response["message"] = $_SESSION['apiError'];
				   $response["mailSent"] = false;
				
				   return $response;
				
				});
				
					
					//
					//add_action( 'wpcf7_form_response_output', 'wpse_form_response_output', 10, 4 );					  
					
					//wpse_form_response_output(); 
					//display error JS popup
    				//add_action( 'wp_footer', 'sw_gf_js_error' );die();
    				
				}
				
				
				//print_r($resultApi1);
				//die;
				
				curl_close($curl);
			} 
			
			//$thankCareer = site_url()."/thank-you-career/?id=".get_the_ID();
			
			//header("Location: $thankCareer");
			
			/*
			

            // extract posted data for example to get name and change it
            $name         = isset($data['your-name']) ? $data['your-name'] : "";

            // do some replacements in the cf7 email body
            $mail         = $wpcf7->prop('mail');

            // Find/replace the "[your-name]" tag as defined in your CF7 email body
            // and add changes name
            $mail['body'] = str_replace('[your-name]', $name . '-tester', $mail['body']);

            // Save the email body
            $wpcf7->set_properties(array(
                "mail" => $mail
            ));
*/
			//$wpcf->skip_mail = true;  
			
            // return current cf7 instance
          return $wpcf7;
        }
    }

    
    // For refer a friend job submit form
	if ( 12427 == $WPCF7_ContactForm->id() || 10398 == $WPCF7_ContactForm->id() || 11796 == $WPCF7_ContactForm->id() || 10459 == $WPCF7_ContactForm->id() ) {

        //Get current form
        $wpcf7      = WPCF7_ContactForm::get_current();
		
		// get current SUBMISSION instance
        $submission = WPCF7_Submission::get_instance();

        // Ok go forward
        if ($submission) {

            // get submission data
            $data = $submission->get_posted_data();
			 $data['uploaded_files'] = $submission->uploaded_files();
	
			//die;
            // nothing's here... do nothing...
            if (empty($data))
                return;
			
			$tmpfile = $data['uploaded_files']['referresume'];
			
			$path_parts = pathinfo($data['referresume']);
		    $filename = $path_parts['basename']; 
		    $filetype = $path_parts['extension'];
			
			$data1 = array(
			'first_name'=>$data['firstname'],
			'last_name'=>$data['lastname'],
			"email"=>$data['email'],
			"mobile"=>$data['phoneNumber'],
			"resume"=>curl_file_create($tmpfile, mime_content_type($filetype), $filename),
			"location"=>$data['mailLoc'],
			"irc_number"=>$data['mailIRC'],
			"gl_website_referred_by_name"=>$data['referName'],
			);
			
			if(isset($data['referralId']) && $data['referralId']!="")
				$data1['refid ']=$data['referralId'];
			 
	
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "https://glo-staging.globallogic.com/gloapis/v1/glwebsite/candidates");
			curl_setopt($curl,CURLOPT_USERPWD,"avohi.gl:Roman@321##");
			curl_setopt($curl, CURLOPT_POST, true);
			
			//$headers = array("content-type: multipart/form-data"); // cURL headers for file uploading

			//curl_setopt($curl, CURLOPT_HEADER, true);
			//curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpfile));
			
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
	
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data1);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$result222 = curl_exec($curl);
			if (curl_errno($curl))
			{
				//print "Error: " . curl_error($curl);die;
			}
			else{				
				//var_dump($result222);die;
				
				$resultApi = json_decode($result222);
				
				if(isset($resultApi->error)){
					$resultApi1 = json_decode(json_encode($resultApi->error),true);
					$apiMessage = $resultApi1['message'];
					$apiReason = $resultApi1['reason'];
					$_SESSION['apiError1'] = "";
					$_SESSION['apiError1'] = $apiMessage."<br>".$apiReason;
				 			 
				add_filter("wpcf7_ajax_json_echo", function ($response, $result, $apiError) {
				
				
				   $response["message"] = $_SESSION['apiError1'];
				   $response["mailSent"] = false;
				
				   return $response;
				
				});
				
				}
				
				curl_close($curl);
			} 
			
			//$thankCareer = site_url()."/thank-you-career/?id=".get_the_ID();
			
			//header("Location: $thankCareer");
			
			$wpcf->skip_mail = false;  
			
            // return current cf7 instance
            return $wpcf7;
        }
    }
	
}

// define the wpcf7_skip_mail callback 
function filter_wpcf7_skip_mail( $skip_mail, $contact_form ) { 
    // make filter magic happen here... 
    return true; 
}; 

function kjokt(){
	return add_filter( 'wpcf7_form_response_output', 'wpse_form_response_output', 10, 1);
}
function wpse_form_response_output( $output, $class, $content, $object )
{
    return sprintf(
        '<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ng" 
            role="alert" style="display: block;">%s</div>',
        __( 'SOAP ERROR - Mail not sent!' )
    );
}
function sw_gf_js_error() {
    ?>
    <script type="text/javascript">
        alert( "test error" );
    </script>
    <?php
}
?>


<?php

    function add_last_nav_item($items,$args) {
     if( $args->theme_location == 'primary' ){
     $items .= '<div class="nav-country-selector">
                <div class="nav-country-selector-world nav-country-selector-toggle">
                  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="523.7 350.7 100 100" style="enable-background:new 523.7 350.7 100 100;" xml:space="preserve">
                  <g>
                   <g>
                     <path class="st0" d="M573.7,350.7c-27.6,0-50,22.4-50,50c0,27.6,22.4,50,50,50s50-22.4,50-50C623.7,373.1,601.3,350.7,573.7,350.7
                       z M573.7,446.5c-25.3,0-45.8-20.5-45.8-45.8c0-25.3,20.5-45.8,45.8-45.8s45.8,20.5,45.8,45.8C619.5,425.9,598.9,446.5,573.7,446.5
                       z"/>
                     <path class="st0" d="M552.5,388.6h-10.3c-1,2.6-1.7,5.5-2,8.4h11.5C551.7,394.1,552,391.3,552.5,388.6z"/>
                     <path class="st0" d="M546.1,381.3h8c0.9-3.1,2-6.1,3.3-8.7c0.3-0.7,0.7-1.3,1.1-2C553.5,373.1,549.3,376.8,546.1,381.3z"/>
                     <path class="st0" d="M594.9,412.7h10.3c1-2.6,1.7-5.5,2-8.4h-11.5C595.6,407.2,595.3,410,594.9,412.7z"/>
                     <path class="st0" d="M595.7,397h11.5c-0.3-2.9-1-5.7-2-8.4h-10.3C595.3,391.3,595.6,394.1,595.7,397z"/>
                     <path class="st0" d="M590,428.7c-0.3,0.7-0.7,1.3-1.1,2c4.9-2.5,9.1-6.2,12.3-10.7h-8C592.4,423.2,591.3,426.1,590,428.7z"/>
                     <path class="st0" d="M593.3,381.3h8c-3.2-4.5-7.4-8.1-12.3-10.7c0.4,0.6,0.7,1.3,1.1,2C591.3,375.2,592.4,378.1,593.3,381.3z"/>
                     <path class="st0" d="M554.1,420.1h-8c3.2,4.5,7.4,8.1,12.3,10.7c-0.4-0.6-0.7-1.3-1.1-2C556,426.1,554.9,423.2,554.1,420.1z"/>
                     <path class="st0" d="M551.6,404.3h-11.5c0.3,2.9,1,5.7,2,8.4h10.3C552,410,551.7,407.2,551.6,404.3z"/>
                     <path class="st0" d="M577.3,368.2v13.1h8.3C583.6,374.8,580.5,370.2,577.3,368.2z"/>
                     <path class="st0" d="M561.7,381.3h8.3v-13.1C566.8,370.2,563.8,374.8,561.7,381.3z"/>
                     <path class="st0" d="M559.9,412.7H570v-8.4h-11C559.1,407.3,559.4,410.1,559.9,412.7z"/>
                     <path class="st0" d="M559,397h11v-8.4h-10.1C559.4,391.2,559.1,394,559,397z"/>
                     <path class="st0" d="M577.3,388.6v8.4h11c-0.1-3-0.5-5.8-0.9-8.4H577.3z"/>
                     <path class="st0" d="M577.3,412.7h10.1c0.5-2.6,0.8-5.4,0.9-8.4h-11L577.3,412.7L577.3,412.7z"/>
                     <path class="st0" d="M570,433.2v-13.1h-8.3C563.8,426.5,566.8,431.1,570,433.2z"/>
                     <path class="st0" d="M577.3,433.2c3.2-2.1,6.2-6.7,8.3-13.1h-8.3V433.2z"/>
                   </g>
                 </g>
                 </svg>
                </div>
                <div class="nav-country-selector-options">
                  <div class="nav-country-selector-toggle hide-for-large"></div>
                    <div class="nav-country-selector-options-wrapper">
                    <a href="https://www.globallogic.com" class="nav-country-selector-option ">
                      <span>Global </span> /  English 
                    </a>
                    <a href="https://www.globallogic.com/latam" class="nav-country-selector-option ">
                      <span>Latam </span> /  Español 
                    </a>
                    <a href="https://www.globallogic.com/ua/" class="nav-country-selector-option ">
                      <span>Ukraine </span> /  українська </a>
                    <a href="https://www.globallogic.com/pl/" class="nav-country-selector-option ">
                      <span>Poland </span> /  English 
                    </a>
                    <a href="https://www.globallogic.com/il/" class="nav-country-selector-option ">
                      <span>Israel </span> /  English 
                    </a>                  
                  </div>
                </div>
              </div>';
  }   
  return $items;
  }
  add_filter('wp_nav_menu_items','add_last_nav_item',10,2);           




function my_cronjob_action () {
    // code to execute on cron run
} add_action('my_cronjob_action', 'my_cronjob_action');

// add the ajax fetch js

function ajax_fetching() {
?>
<script type="text/javascript">
alert('yesy sdfsdf');
</script>

<?php
}


?>
<?php
function pippin_add_taxonomy_filters() {
  global $typenow;
  
  // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
  $taxonomies = array('news-category');
  
  // must set this to the post type you want the filter(s) displayed on
  if( $typenow == 'news' ){
    
    foreach ($taxonomies as $tax_slug) {
      $tax_obj = get_taxonomy($tax_slug);
      $tax_name = $tax_obj->labels->name;
      $terms = get_terms($tax_slug);
      if(count($terms) > 0) {
        echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
        echo "<option value=''>Show All $tax_name</option>";
        foreach ($terms as $term) { 
          echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
        }
        echo "</select>";
      }
    }
  }
}
add_action( 'restrict_manage_posts', 'pippin_add_taxonomy_filters' );