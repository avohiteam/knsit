<?php
/**
  Template Name: News
 */

get_header();
$page_size = 9;
$page_param = filter_input(INPUT_GET, 'page');
$page = get_query_var( 'page', 1 ) ;
$ajax = ($page > 1) ? true : false;
print_hero($post->ID);
print_subnav('who we are');

$embedded_type = get_term_by('slug', 'news', 'news-category');

$press_kit_category = get_term_by('slug', 'press-kit', 'news-category');



$base = get_post_meta($post->ID, 'news_type', true);


$embedded_cat = get_term_by('slug', $base, 'news-category');
//$embedded_type=get_post_meta($post->ID, 'embedded_type', true);
//$category = get_term_by('slug',$embedded_type , 'embedded-category');
//print_r($category);
//$filters = get_post_meta($post->ID, 'embedded_category_filters', true);
$all_filter = get_post_meta($post->ID, 'show_filter_all', true);
$args = array(
    'posts_per_page' => $page_size,
    'paged' => $page, 
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'news',
 
);
$embeddedlist = query_posts($args);

//print_r($embeddedlist);



  $filters = get_terms(array(
      'taxonomy' => 'news-category',
      'hide_empty' => 0,
        'childless' => true,
  ));

$filter_columns = count($filters);
  $init_tab = $filters[0]->slug;
  if($all_filter){
    //plus 1 for ALL option in filters
    $filter_columns++;
    $init_tab = 'all';
  }


   $show_press_kit = true;
    $press_kit = query_posts(
    array(
    'post_type' => 'news',
    'tax_query' => array(
        array (
            'taxonomy' => 'news-category',
            'field' => 'slug',
            'terms' => 'press-kit',
        )
    ),
));




    if(count($press_kit)<=0){
      $show_press_kit = false;
    }

if ($filters) {
  
  $filter_columns = ($show_press_kit)? count($filters) + 1 : count($filters);
  $init_tab = $filters[0]->slug;
  if($all_filter){
    //plus 1 for ALL option in filters
    $filter_columns++;
    $init_tab = 'all';
  }
  
  ?>
  <div class="news-grid-wrapper" data-equalizer data-gl-tabs data-init-tab="<?php echo $init_tab?>">
    <div class="row">
      <div class="news-filters-wrapper show-for-medium">
        <!--<div class="row medium-up-<?php //echo $filter_columns ?> collapse"> -->
		<div class="row medium-up-5 collapse">
          <?php
          if($all_filter){
            ?>
              <div class="column">
                <a href="#" class="news-filter" data-show-tab="all"><?php echo of_get_option('general_all_text'); ?></a>
              </div>
              <?php
          }
          foreach ($filters as $filter) {
             if($filter->slug == 'press-kit') {continue;}
            ?>
            <div class="column">
              <a href="#" class="news-filter" data-show-tab="<?php echo $filter->slug ?>"><?php echo $filter->name ?></a>
            </div>
            <?php
          }
          if ($show_press_kit) {
            ?>
            <div class="column">
              <a href="#" class="news-filter" data-show-tab="press-kit"><?php echo $press_kit_category->name ?></a>
            </div>
            <?php
          }
          ?>

        </div>
      </div>
      <div class="small-12 columns news-grid-tab mod-all" data-gl-tab-content="all">
        <div class="row medium-up-2 large-up-3 news-grid">
          <?php
          foreach ($embeddedlist as $new) {
            ?>
            <div class="column">
              <?php print_news_card($new, $category_base, $show_images) ?>
            </div>
            <?php
          }
          ?>
        </div>
        <?php 
          if(count($embeddedlist)== $page_size) {
            ?>
              <button class="btn mod-orange" data-loading-text="<?php echo of_get_option('general_loading_text'); ?>..." data-load-more data-page-size="<?php echo $page_size; ?>" data-news-grid="[data-gl-tab-content='all'] .news-grid" data-url="<?php echo get_permalink($post->ID)?>" data-page="<?php echo $page; ?>">
                <?php echo of_get_option('general_load_more_text'); ?>
              </button>
              <?php
          }
          ?>
        
      </div>
      <?php
      foreach ($filters as $filter) {
        if($filter->slug == 'press-kit') {continue;}
        ?>
        <div class="small-12 columns news-grid-tab" data-gl-tab-content="<?php echo $filter->slug ?>">
          <div class="row medium-up-2 large-up-3 news-grid">
            <?php
            $args = array(
                'posts_per_page' => $page_size,
                'paged' => $page,
                'orderby' => 'date',
                'order' => 'DESC',
                'post_type' => 'news',
                'tax_query' => array(
        array(
            'taxonomy' => 'news-category',
            'field' => 'slug',
            'terms' => $filter->slug,
        ),
    ),
            );
            $newslist = query_posts($args);
            foreach ($newslist as $new) {
              ?>
              <div class="column">
                <?php print_news_card($new, $category_base, $show_images) ?>
              </div>
              <?php
            }
            ?>
          </div>
          <?php 
          if(count($newslist)== $page_size) {
            ?>
              <button class="btn mod-orange" data-load-more data-loading-text="<?php echo of_get_option('general_loading_text'); ?>..." data-page-size="<?php echo $page_size; ?>" data-news-grid="[data-gl-tab-content='<?php echo $filter->slug; ?>'] .news-grid" data-url="<?php echo get_permalink($post->ID)?>" data-page="<?php echo $page; ?>">
                <?php echo of_get_option('general_load_more_text'); ?>
              </button>
              <?php
          }
          ?>
          
        </div>
        <?php
      }
      
      
    if ($show_press_kit) {
        ?>
        <div class="small-12 columns news-grid-tab" data-gl-tab-content="press-kit">
          <div class="row">
            <div class="medium-10 medium-centered columns text-left">

              <?php echo $press_kit[0]->post_content; ?>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
  <?php
} else {
  ?>

  <div class="news-grid-wrapper" data-equalizer data-gl-tabs data-init-tab="<?php echo $init_tab?>">
   


    <div class="row">
      <div class="small-12 columns">
        <div class="row medium-up-2 large-up-3 news-grid">
          <?php
          foreach ($embeddedlist as $new) {
            print_r($new);
            ?>
            <div class="column">
              <?php print_news_card($new, $category_base, $show_images) ?>
            </div>
            <?php
          }
          ?>
        </div>
        <?php 
          if(count($newslist) == $page_size) {
            ?>
            <button class="btn mod-orange" data-load-more data-loading-text="<?php echo of_get_option('general_loading_text'); ?>..." data-page-size="<?php echo $page_size; ?>" data-news-grid=".news-grid" data-url="<?php echo get_permalink($post->ID)?>" data-page="<?php echo $page; ?>">
              <?php echo of_get_option('general_load_more_text'); ?>
            </button>
            <?php
          }
          ?>
        
      </div>
    </div>
  

  </div>

<?php } ?>
<?php
echo "<p class='common-message'>".of_get_option('general_common_message')."</p>";
get_footer();
?>